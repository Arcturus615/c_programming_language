/*
 *  Exercise 1.21:
 *  Write a program `entab` that replaces strings of blanks by the minimum number of tabs and blanks
 *  to acheive the same spacing. Use the tab stops as for detab. When either a tab or a single blank
 *  would suffice, which should be given preference?
 */

#include <stdio.h>

#define MAXLINE	1000
#define TABSTOP	8

int get_line(int line[], int char_limit, int tabstop);
void print_line(int line[], int char_limit);
void reset_arr(int line[], int char_limit);

int main ()
{

	int limit = MAXLINE;
	int tabstop = TABSTOP;

	int line[limit];
	int len;

	while ((len = get_line(line, limit, tabstop)) > 0)
		print_line(line, limit);

	return 0;
}

int get_line (int s[], int lim, int tabstop)
{
	reset_arr(s, lim);
	
	int blanks = 0;
	int tabs = 0;
	int c, n, i, len, whtspc;

	n = i = len = whtspc = 0;

	while ((c = getchar()) != '\n' && c != EOF)
	{
		if (c == ' ')
			blanks++;
		else if (c == '\t')
			tabs++;
		else
		{
			whtspc = blanks + (tabs * tabstop);

			for ( n = whtspc / tabstop; n > 0; n--)
			{
				s[i] = '>';
				len++;
				i++;
			}

			for ( n = whtspc % tabstop; n > 0; n--)
			{
				s[i] = '_';
				len++;
				i++;
			}

			s[i]=c;

			tabs = blanks = whtspc = 0;

			len++;
			i++;
		}
	}
	return len;
}

void print_line (int s[], int lim)
{
	int i;
	
	for (i=0;i <= lim && s[i] != '\0'; i++)
		printf("%c", s[i]);
	printf("\n");
}

void reset_arr(int s[], int lim)
{
	int i;

	for (i=0; i <= lim; i++ )
		s[i] = '\0';
}
