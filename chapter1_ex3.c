#include <stdio.h>

/*
 * print Fahrenhei-Celsius table
 * for fahr = 0, 20, ..., 300
 */

main() {
	float fahr, celsius;
	float lower, upper, step;
	
	printf("Fahrenheit to Celsius table:");
	printf("----------------------------");
	
	lower = 0; /* lower limit of temperature table*/
	upper = 300; /* upper limit */
	step = 20; /* step size */

	fahr = lower;
	while (fahr <= upper) {
		//celsius = 5 * (fahr-32) / 9; old int algorithm
		celsius = (5.0/9.0) * (fahr-32.0);
		printf("%3.0f %6.1f\n", fahr, celsius);
		fahr = fahr + step;
	}
}
