/*
 *	Exercise 1-13 Histogram
 */

#include <stdio.h>

#define IN	1	// Define IN global as 1
#define OUT	0	// Define OUT global as 0

main () {
	int c, i, i2, state, count[32767];
	int words, newlines, total_chars, word_char_count, whitespace;
	int max_char_count;

	state = OUT;

	max_char_count = word_char_count = words = total_chars = whitespace = 0;
	while ( (c = getchar()) != EOF ) {
		++total_chars;
		if ( c == ' ' || c == '\t' || c == '\n' ) {
			++whitespace;
			state = OUT;
			if ( word_char_count > 0 ) {
				if ( word_char_count > max_char_count )
					max_char_count = word_char_count;
				++count[word_char_count];
				word_char_count = 0;
			}
		}

		else {
			if ( state == IN ) {
				++word_char_count;
			}

			if ( state == OUT ) {
				state = IN;
				word_char_count = 0;
				++words;
				++word_char_count;
			}
		}
	}

	printf("-------------------------------\n");
	printf("Word character count histogram:\n");
	printf("-------------------------------\n");
	printf("Top count: %d\n", max_char_count);
	printf("Char Total: %d\n", total_chars);
	printf("Total words: %d\n", words);
	printf("-------------------------------\n");

	for ( i = 0; i <= max_char_count; ++i) {
		if ( count[i] > 0) {
			printf("%2d:",i);
			for ( i2=0; i2 < i; ++i2  )
				printf("=");
			printf("|\n");
		}
	}
}
