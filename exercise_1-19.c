/*
 * Exercise 1.19:
 * 	Write a function reverse(s) that reverses the character string (s). Use it to write a program that reverses it's input a line at a time
 */

#include <stdio.h>

#define MAXINPUT	1000

int get_line(char line[], int limit);
int reverse(char line[], char rev_line[], int limit);
int print_line(char line[], int limit);

int main()
{
	// Initialize the line and rline tables with *all* values set to 0
	char line[MAXINPUT] = { 0 };
	char rline[MAXINPUT] = { 0 };
	int len;

	len = 0;

	// Ensuring that the stdin is greater that 0, run "reverse"
	// on the input, and then print
	while((len = get_line(line, MAXINPUT)) > 0)
	{
		reverse(line, rline, MAXINPUT);
		print_line(rline, MAXINPUT);
	}

	return 0;
}

// Using stdin, determine the current operative line and store it in the
// "line" char array 
int get_line(char s[] , int limit)
{	
	int i, c;
	int len;

	len = i = 0;

	for (i=0; i < limit && (c = getchar()) != EOF && c != '\n'; ++i)
	{
		len++;
		s[i] = c;
	}

	if (c == EOF || c == '\n')
	{
		i++;
		s[i] = '\0';
	}

	return len;	
}

// Reverse the char array provided a line at a time
int reverse(char s[], char rs[], int limit)
{
	int i, c, ri, len;
	len = ri = 0;

	for (i=limit; i >= 0; i--)
	{
		len++;
		c = s[i];

		if (c != '\n' && c != '\0' && c != 0)
		{
			//printf("%d: %c\n", ri, c);
			rs[ri] = c;
			ri++;
		}
	}
	
	rs[ri] = '\0';
	return len;
}

// Printf the passed char array to the screen, followed by a newline
int print_line(char s[], int limit)
{
	int i, c;

	for (i=0; i < limit && (c = s[i]) != EOF && c != '\0' && c != '\n'; ++i)
		printf("%c", c);
	
	printf("\n");
	return 0;
}
