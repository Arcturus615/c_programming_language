#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXFILES 50
#define MAXPATH 100
#define DEBUGMODE 0
#define MAXARG 255
#define NUMARGS 2

#define IN 1
#define OUT 0
#define TRUE 1
#define FALSE 0

#define IN_MCOM 1
#define IN_SCOM 2
#define IN_SQUT 3
#define IN_DQUT 4

/* 
 * get_arg_type:
 * 	Return the int ID matching the arg passed
 *
 * Arguments:
 * 	char *arr: Char array containing the arguemtn to check
 * 	
 * 	char (*args)[NUMARGS][MAXARG]: String array containing 
 * 	the valid argument switches
 *  
 * Returns:
 * 	int 0 == input file
 * 	int 1+ == argument switch ID	
 */
int get_arg_type ( char *arr, char (*args)[NUMARGS][MAXARG] );

/*
 * process_file:
 * 	Process the file being passed.
 *
 * Arguments:
 *  FILE *in: Pointer to input file
 *
 *  FILE *out: Pointer to output file
 *  	NOTE: Not necessarily a referenced pointer
 *  
 *  int mode: Int defining whether or not to write to
 * 	FILE *out or stdout
 *
 * Returns:
 * 	none
 */
int process_file ( FILE *in, FILE *out, int mode );

/*
 * exists:
 * 	Check whether or not a file exists
 * 
 * Arguments:
 * 	char *filename: Char array containing the filepath
 *
 * Returns:
 * 	int 0 == True
 * 	int 1 == False
 */
int exists ( char *filename );

/* 
 * printn:
 *	Print the passed char array and a trailing newline		
 * 
 * Arguments:
 * 	char *str: Char array
 *
 * Returns:
 * 	void
 */
void printn ( char *str );

/*
 * die:
 * 	Print a char array and exit with code 1
 *
 * Arguments:
 * 	char *str: Char array
 *
 * Exits:
 *  1
 */
void die ( char *str );

int main ( int argc, char **argv )
{
	// i: Ununsed
	// f: Count of files to be processed
	// mode: Pass into process_file.
	// 		0 == Print to stdout
	// 		1 == Append to file
	// out_path: Char array defining output file
	// in_paths: 2D Char array with input file paths
	// valid_args: 2D Char array where valid switches are defined
	int i, f, mode;
	char out_path[MAXPATH];
	char in_paths[MAXFILES][MAXPATH];
	char valid_args[NUMARGS][MAXARG];

	// cur_file: Pointer to current *input* file
	// out_file: Pointer to operative *output* file
	FILE *cur_file;
	FILE *out_file;

	// Add the argument strings to the valid_args array
	strcpy( (valid_args)[0], "-o" );

	f = 0;
	mode = 0;

	for ( i=1; i < argc; i++ )
	{
		if ( DEBUGMODE == TRUE )
			printf( "Operating on argument %d: %s\n", i, (argv)[i] );
	
		switch ( get_arg_type((argv)[i], &valid_args) )
		{
			// case 0: Define output file
			case 0:
				if ( (argv)[i+1] )
				{
					if ( DEBUGMODE == TRUE )
						printf( "Output File: %s\n", (argv)[i+1] );
					strcpy( out_path, (argv)[i+1] );
				}

				else
				{
					printf( "No Output file specified!\n" );
					exit(1);
				}
				i++;
				break;

			// default: Attempt to define new input file from argument,
			// 			iterate "int f" if successful, and "exit 1" if
			// 			not.
			default:
				strcpy( (in_paths)[f], (argv)[i] );
				if ( DEBUGMODE == TRUE )
					printf( "Added %s to inputs\n", in_paths[f] );
				f++;
				break;
		}

		if (i < argc - 1)
			putchar('\n');
	}

	// With f, again, being the count of files to be processed,
	// open the operative input file, and process it.
	for ( i=0; i<f; i++ )
	{
		if ( exists((in_paths)[i]) == 0 )
		{
			cur_file = fopen( (in_paths)[i], "r" );
			process_file(cur_file, out_file, mode);
			fclose( cur_file );
		}

		else
		{
			printf( "File %s doesn't exist or is unreadable\n", in_paths[i] );
			exit(1);
		}
	}
	return 0;
}

int get_arg_type ( char *arr, char (*args)[NUMARGS][MAXARG] )
{
	int i;
	for ( i=0; i<NUMARGS; i++ )
		if ( strcmp(arr,(*args)[i]) == 0 )
			return i;
	return -1;
}


int exists ( char *file )
{
	FILE *f;
	if ( f = fopen( file , "r") )
	{
		fclose(f);
		return 0;
	}
	return 1;
}

int process_file ( FILE *in, FILE *out, int mode )
{
	int skip, state, c_type, q_type;
	char c, q, pc, nc;

	state = OUT;
	nc = 0;
	c = fgetc( in );

	while ( c != EOF )
	{
		// Default skip to FALSE
		skip = FALSE;
		
		// If the current state is neither
		// in a comment or a single/double
		// quote, then check the current char.
		// If it's a "/" then check whether or
		// not the next char is defining a single
		// line or multiline comment. Drop everything
		// for a single line until the next newline.
		// For a multiline comment, continue with state
		// IN_MCOM.
		//
		// The same thought pattern applies to quotes.
		if ( state == OUT )
		{
			if ( c == '/' )
			{
				nc = fgetc( in );

				if ( nc == '*' )
					state = IN_MCOM;
				
				else if ( nc == '/' )
				{
					nc = 0;
					while ( c != '\n' )
						c = fgetc( in );
				}
			}

			else if ( c == '\'' )
				state = IN_SQUT;

			else if ( c == '"' )
				state = IN_DQUT;
		}

		else if ( state == IN_MCOM )
		{
			if ( c == '*' )
			{
				nc = fgetc( in );

				if ( nc == '/' )
				{
					// Set skip to true here
					// to prevent the ending
					// comment chars from being
					// output
					state = OUT;
					skip = TRUE;
				}
			}
		}

		else if ( state == IN_SCOM )
		{
			if ( c == '\n' )
				state  = OUT;
		}

		else if ( state == IN_DQUT )
		{
			if ( c == '"' )
				state = OUT;
		}

		else if ( state == IN_SQUT )
		{
			if ( c == '\'' )
				state = OUT;
		}
	
			
		if ( skip == FALSE )
		{
			if ( state != IN_MCOM && state != IN_SCOM )
			{
				if ( state == OUT || state == IN_DQUT || state == IN_SQUT )
				{
					if ( mode == 0 )
					{
						putchar( c );
						if ( nc != OUT && nc != EOF )
							putchar( nc );
					}

					else if ( mode == 1 )
					{
						fputc( c, out );
						if ( nc != OUT && nc != EOF )
							fputc( nc, out );
					}

					else
					{
						printf( "Processing mode exceeds bounds 0-1: (%d)", mode );
						fclose( in );
						fclose( out );
						exit(1);
					}
				}
			}

			else if ( state != IN_MCOM && state != IN_SCOM )
			{
				printf( "Processing state exceeds bounds %d-%d: (%d)", OUT, IN_DQUT, state );
				fclose( in );
				fclose( out );
				exit(1);
			}
		}

		if ( nc == EOF )
			c = EOF;
		else
		{
			nc = 0;
			c = fgetc( in );
		}
	}	
}

void printn ( char *str )
{
	printf( "%s\n", str );
}

void die ( char *str )
{
	printn( str );
	exit(1);
}
