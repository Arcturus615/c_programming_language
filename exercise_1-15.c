/*
 *
 * Temperature conversion rewrite
 *
 *	Temperature scale IDs:
 *		Celsius		'0'
 *		Fahenheit	'1'
 *		Kelvin		'2'
 */

#include <stdio.h>

#define	UPPER	200
#define	LOWER	0
#define	STEP	10
#define	SCALE	'1'


int tableprint (int scale);
float converttofahr ( float temp, int scale );
float converttocels ( float temp, int scale );
float converttokelv ( float temp, int scale );

int main ()
{
	return tableprint(SCALE);
}

int tableprint (int scale)
{	
	printf("-----------------------------\n");
	printf("Generating temperature table:\n");
	printf("-----------------------------\n\n");
	printf("%8s %8s %8s\n",
							"[Cels.]",
							"[Fahr.]",
							"[Kelv.]"
							);

	int i;
	for (i = LOWER; i <= UPPER; i = i + STEP)
	{
		printf("%8.1f %8.1f %8.1f\n",
									converttocels(i, scale),
									converttofahr(i, scale),
									converttokelv(i, scale)
									);
	}

	return 0;
}

float converttokelv (float temp, int scale)
{
	if ( scale == '0' )
		return temp + 273.15;
	else if ( scale == '1')
		return converttocels(temp, scale) + 273.15;
	else if ( scale == '2' )
		return temp;
	else
		return 3;
}

float converttocels (float temp, int scale)
{
	if ( scale == '0' )
		return temp;
	else if ( scale == '1' ) 
		return (5.0/9.0)*(temp-32.0);
	else if ( scale == '2' )
		return temp - 273.15;
	else
		return 3;
}

float converttofahr (float temp, int scale)
{
	if( scale == '0' )
		return (temp*(9.0/5.0))+32.0;
	else if ( scale == '1' )
		return temp;
	else if ( scale == '2' )
		return ( (temp - 273.15) *(9.0/5.0))+32.0;
	else
		return 3;
}
