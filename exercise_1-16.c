/*
 *  Chapter 1.9 Example 1;
 *	Goal: Determine the longest string in an array
 *	
 *	Outline:
 *		while (theres another line)
 *			if (its longer that the prebious longest)
 *				save it
 *				save its length
 *		print the longest line
 */

#include <stdio.h>
#define MAXLINE	1000 // maximum input line size

int mygetline(char line[], int maxline);
void copy(char to[], char from[]);

// Print longest input line
int main ()
{
	int len;	// Current line length
	int max;	// Maximum length seen so far
	char line[MAXLINE]; 	// Current input line
	char longest[MAXLINE];	// Longest line saved

	max = 0;
	while ((len = mygetline(line, MAXLINE)) > 0 )
		if (len > max)
		{
			max = len;
			copy(longest, line);
		}
	if (max > 0)
		printf("%d: %s", max,longest);
	return 0;
}


// getline: Read a line into s, return length
int mygetline(char s[], int lim)
{
	int c, i;
	for (i=0; i<lim-1 && (c=getchar()) != EOF && c!='\n'; ++i)
		s[i] = c;
	if (c == '\n')
	{
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}

// copy: copy 'from' into 'to'; assume to is big enough
void copy (char to[], char from[])
{
	int i;

	i = 0;
	while ((to[i] = from[i]) != '\0')
		++i;
}
