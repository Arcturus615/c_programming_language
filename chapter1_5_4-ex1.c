#include <stdio.h>

#define IN	1	//Inside a word
#define OUT	0	//Outside a word

// Count lines, words, and characters in input

main () {
	int c, nl, nw, nc, state; // Declare variables

	state = OUT;		// Set the state to OUT, so, outside of a word
	 nl = nw = nc = 0;	// Set all vars to 0

	 while ((c = getchar()) != EOF) {	// Get the current char, and loop
	 	++nc;							// Iterate char number
		if (c == '\n')					// If char is a newline
			++nl;						// iterate
		if (c == ' ' || c == '\n' || c == '\t')	// If char is spcae, tab, or
			state = OUT;						// newline, set state to OUT
		else if (state == OUT) {		// If none of the above is true
			state = IN;					// set state to IN and
			++nw;						// iterate word count
		}
	 }

	printf("%d %d %d\n", nl, nw, nc);

}
