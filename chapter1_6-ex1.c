#include <stdio.h>

// Count digits, white space, others
main () {
	int c, i, nwhite, nother;	// Define variables as int
	int ndigit[10];				// Define an array of 10 ints as ndigit

	nwhite = nother = 0;		// Set nwhite and nother to 0
		for (i = 0; i < 10; ++i)	// For 10 iterations (i)
			ndigit[i] = 0;			// set an int within ndigit (i) to 0
		
	while ((c = getchar()) != EOF)	// While the operative char is NOT EOF
		if (c >= '0' && c <= '9')	// if the operative char is 0-9
			++ndigit[c-'0'];		// iterate that digit within ndigit
		else if (c == ' ' || c == '\n' || c == '\t') // Else, if blankspace
			++nwhite;				// iterate nwhite
		else
			++nother	// Otherwise, iterate nother

	printf("digits =");	// Print digits **without** a newline
		for (i = 0; i < 10; ++i)					// For 0-9,
			printf(" %d", ndigit[i]);				// print digit count
		printf(", white space = %d, other = %d\n",	// Finally, print
				nwhite, nother);					// nwhite and nother
}
