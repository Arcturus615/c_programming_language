#include <stdio.h>

// Print stdin to stdout, truncating blankspaces
main () {
	int c;
	long bl;
	
	bl = 0;
	while((c = getchar()) != EOF) {
		if(c == ' ') {				// Catch space chars
			++bl;					// Iterate number of spaces
			if(bl == 1)				// Only allow one space to be
				printf("%c", ' ');	// printed
		}

		if(c == '\t') {				// Same as above, but adds
			++bl;					// tabs to the list of chars
			if(bl == 1)				// caught as a space
				printf("%c", ' ');	//
		}

		if(c != ' ') {				// If a char isnt a space
			if( c != '\t') {		// or a tab
				bl = 0;				// Set the space count to 0
				printf("%c", c);	// and print the char
			}
		}
	}	
}
