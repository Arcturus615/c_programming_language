#include <stdio.h>


// Count all the newlines, tabs, and spaces in stdin
main () {
	int c;			// Declare c as int, to hold ONE number
	long bl,nl,tb;	// Declare others as long, for large input
	
	bl = 0;
	nl = 0;
	tb = 0;
	
	while ((c = getchar()) != EOF) {	
		if (c == '\n')	// Count newline 
			++nl;		//

		if (c == ' ')	// Count space
			++bl;		//

		if (c == '\t')	// Count tab
			++tb;		//
	}

	printf("Input contains the following:\n");
	printf("Newlines: %ld\nBlanks: %ld\nTabs: %ld\n", nl, bl, tb);
	
	return 0;	// Return successful
}
