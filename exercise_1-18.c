/*
 * Exercise 1-18: Write a program to remove trailing blanks and tabs from each line of input, and to 
 * delete entirely blank lines
 */


#include <stdio.h>

#define MAXLINE	1000	// Define a maximum char count of 1000
#define IN	1			// Define a state of "IN" a word
#define OUT	0			// Define a state of "OUT" a word

int print_line (char input[], int limit);
int get_line (char line[], int limit);

int main ()
{
	char line[MAXLINE];
	int len;

	len = 0;
	while ((len = get_line(line, MAXLINE)) > 0)
		print_line(line, MAXLINE);
}

int get_line (char s[], int limit)
{
	int i, c;
	int len, words, state, blanks;
	
	len = 0;
	words = 0;
	blanks = 0;
	state = OUT;

	for (i=1; i < limit-1 && (c = getchar()) != EOF && c != '\n'; ++i)
	{
		++len;
		if ( c == ' ' || c == '\t')
		{
			// Check whether or not the char received is a space or a tab
			// if so, iterate the count by 1 and set the char added to be
			// a space. Finally, set the state to OUT signifying that
			// there is no word currently being operated on.	
			++blanks;
			if (blanks == 1)
				s[i] = ' ';
			if (state == IN)
				state = OUT;
		}
		else
			// If the char received is NOT a blank space, set blanks to 0
			// If we are currently NOT operating on a word, set state to
			// IN indicating that we are. Then, iterate words by 1
			blanks = 0;
			if (state == OUT)
			{
				state = IN;
				++words;
			}
	}
	if (c = '\n')
	{
		if (s[i-1] == ' ')
		{
			// If the previous character in the array is a blank space,
			// set that character to '\n', set this character to '\0'
			// and return
			s[i-1] = '\n';
			s[i] = '\0';
			return words;
		}
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return len;
}

int print_line (char input[], int limit)
{
	int i, c;
	for (i=1; i < limit - 1 && (c = input[i]) != '\n' && c != '\0'; ++i )
		printf("%s", c);
}
