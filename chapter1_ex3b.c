#include <stdio.h>

#define LOWER	0
#define UPPER	100
#define STEP	20

main() {
	int celsius;

	printf("Celsius to Fahrenheit Table:\n");
	printf("----------------------------\n");

	for(celsius = UPPER; celsius >= LOWER; celsius = celsius - STEP)
		printf("%3d %d\n", celsius, (celsius * (9/5)) + 32);
}
