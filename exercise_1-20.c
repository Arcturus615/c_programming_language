/*
 * Exercise 1.20:
 * 	Write a program `detab` that replaces tabs in the input with the proper number of blanks to space to the next tab stop.
 * 	Assume a fixed set of tab stops, say every n columns. Should n be a variable or a symbolic parameter?
 */

#include <stdio.h>

#define MAXLINE		1000;	// Define the maximum allowed input characters
#define TABLIMIT	3;		// Define the limit of tabs to be processed.
#define TABSTOP		8;		// Define the number of characters eauals a tab

int get_line(int line[], int limit);
int check_line(int line[], int processed_line[], int tabset, int limit, int tablimit);
void print_line(int printed_line[], int limit);
void reset_arr(int s[], int limit);

int main(void)
{
	int tabsetting = TABSTOP;
	int limit = MAXLINE;
	int tablimit = TABLIMIT;
	int len = 0;
	int tabcount = 0;
	
	int line[limit];
	int pline[limit];

	reset_arr(line, limit);
	reset_arr(pline, limit);

	printf("Char limit:%d\nTabstop Conf:%d\n", limit, tabsetting);

	while( (len = get_line(line, limit)) > 0)
	{
		tabcount = check_line(line, pline, tabsetting, limit, tablimit);
		print_line(pline, limit);
		printf("%d tabs present.\n", tabcount);
	}
}

// get_line: read from stdin
int get_line(int s[], int limit)
{
	// Reset the s array to ensure previous loops do not contaminate output
	reset_arr(s, limit);

	int i, c, len;

	i = 0;
	len = 0;

	for (i=0; i <= limit && (c = getchar()) != '\n' && c != EOF; ++i)
	{
		len++;
		s[i]=c;
	}

	if (c == EOF || c == '\n')
	{
		i++;
		s[i]='\0';
	}

	return len;
}


// check_line: check the first char array passed for tabs and
// assign the adjusted char table to the second array
int check_line(int s[], int ps[], int tabset, int lim, int tlim)
{
	// Reset the ps array to prevent output contamination
	reset_arr(ps, lim);

	// i: index of original array
	// c: character
	// n: number of spaces to be inserted into the char array
	// pi: index for processed array
	// tcnt: count the number of chars processed this loop
	// cnt: count the number of chars processed so far
	int len, n, c, i, pi, tcnt, cnt;
	c = i = pi = tcnt = cnt = 0;

	while(s[i] != '\0')
	{
		if (s[i] == '\t')
		{
			// Determine the number of spaces to use by getting the
			// modulus (remainder) of the count versus how many chars
			// make up a tab.
			for (n = tabset - (cnt % tabset); n > 0; n--)
			{
				//ps[pi] = '|';
				ps[pi] = '|';
				cnt++;
				pi++;
			}
			tcnt++;
		}
		else
		{
			ps[pi] = s[i];
			pi++;
			cnt++;
		}
		i++;
	}

	return tcnt;
}

// print_line: print passed char array to the screen
void print_line(int s[], int limit)
{
	int c, i;
	
	for(i=0; i < limit && s[i] != EOF && s[i] != '\0'; i++)
		printf("%c", s[i]);
	printf("\n");
}

// reset_arr: clear the passed array by filling all
// indices with "0"
void reset_arr(int s[], int limit)
{
	int i;

	for (i=0; i < limit; i++)
		s[i]=0;
}
