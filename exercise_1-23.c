/*
 * Exercise 1.23
 * Write a program to remvoe all comments from a C program. Don't forget to handle quoted strings
 * and character constants properly. C comments do not nest.
 */

#include <stdio.h>
#include <string.h>

#define MAXFILES 50
#define MAXLINE 100
#define MAXPATH 1000
#define ARGSIZE 255
#define IN 1
#define OUT 0
#define MLINE 0
#define SLINE 1

/* process_args: process program arguements */
int process_args (int argc, char **argv, char input_paths[MAXFILES][MAXPATH], char output_paths[MAXFILES][MAXPATH], int file_count);
int arg_get_id (char *opt, char argument_table[255][3], int possible_args);

/* process_file: process given file and print stdout or an output file */
int process_file (FILE *input_file, FILE *output_file);
void file_print_line (char *line, int size);

/* process_comment: process comments. Assistant functions operate on an array pointer */
int process_comment (FILE *file);
void comment_get_start (char *line, int *comment_positions, int size);
void comment_get_end (FILE *file, int *comment_positions, int start_line, int start_line_pos);

/* init_***_arr: initialize array values to 0 */
void init_int_arr (int array[MAXPATH], int limit, int type);
//void init_char_arr (char array[MAXPATH], int limit, int type);
void init_file_char_arr (char array[], int limit);

/* Globals */
int mode[] = { 0 } ;
extern int mode[];

int main (int argc, char **argv)
{
	int fcnt, cnt, i;
	
	char output_paths[MAXFILES][MAXPATH];
	char file_paths[MAXFILES][MAXPATH];
	

	fcnt = cnt = 0;

	for ( i=0; i<MAXFILES; i++ )
	{
		init_file_char_arr( file_paths[i], MAXPATH );
		init_file_char_arr( output_paths[i], MAXPATH );
	}
	
	if ( (fcnt = process_args(argc, argv, file_paths, output_paths, fcnt)) > 0 )
	{
		FILE *in_files[fcnt];
		FILE *out_files[fcnt];
		for ( i=0; i<fcnt; i++ )
		{
			in_files[i] = fopen(file_paths[i], "r");
				
			if (output_paths[i] != 0)
			{
				out_files[i] = fopen(output_paths[i], "w");
			}

			else
			{
				out_files[i] = fopen(output_paths[0], "w");
				fseek(out_files[i], 0, SEEK_END); //We move to the end here to prevent output loss
			}

			process_file(in_files[i], out_files[i]);

			fclose(in_files[i]);
			fclose(out_files[i]);
		}

		printf("Processed %d file[s]", cnt);
	}

	else
	{
		printf("No files passed!\n");
		return 1;
	}
}

/*
 * process-args: process the arguements passed to the program
 * and operate based on that.
 *
 * Returns:
 * 		0 == Arguement is a file 
 */
int process_args (int argc, char **argv, char input_paths[MAXFILES][MAXPATH], char output_paths[MAXFILES][MAXPATH], int fcnt)
{
	int i, cnt;
	int numargs = 3;
	int argsize = 255;
	
	char arguments[numargs][argsize];	
	
	strcpy(arguments[0], "--show-line-count");
	strcpy(arguments[1], "-o");
	strcpy(arguments[2],  "-oo");
	
	cnt = 0;
	
	for (i = 0; i <= argc; i++)
		switch (arg_get_id(argv[i], arguments, numargs))
		{
			case 1:
				// Show line count on print
				mode[0] = 1;
			case 2:
				i++;
				
				// If no output file was specified yet, assign the given
				// output file as the zero index. This will be used as
				// the default if no output file is specified for other
				// indexed inputs. Going forward, all output files
				// will be assigned to the current cnt index.
				if (output_paths != 0)
					strcpy(output_paths[cnt], argv[i]);
				else
					strcpy(output_paths[0], argv[i]);

			default:
				cnt++;
		}
}


int process_file (FILE *in, FILE *out)
{
	int i;
	int c_type = -1;

	int c_state = OUT;
	int q_state = OUT;

	char c = 0;
	char q = 0;
	char pc = 0;
	
	while ( (c = fgetc(in)) != EOF )
	{
		if ( c_state == OUT )
		{
			if ( c == '"' || c == '\'')
			{
				if (q_state == IN && c == q)
				{
					q = -1;
					q_state = OUT;
				}

				else if (q_state == OUT)
				{
					q = c;
					q_state = IN;
				}
			}

			else if ( c == '/' && pc == '/')
			{
				c_state = IN;
				c_type = SLINE;		
			}

			else if (c == '*' && pc == '/')
			{
				c_state = IN;
				c_type = MLINE;
			}
		}
		
		else if ( c_state == IN )
		{
			if ( c_type == SLINE && c == '\n' )
			{
				c_state = OUT;
				c_type = -1;
				pc = c;
				continue; // Prevent the comment char from being written.
			}

			else if (c_type == MLINE && c == '/' && pc == '*' )
			{
				c_state == OUT;
				c_type = -1;
			}
		}

		if ( c_state == OUT )
			fputc(c, out);

		pc = c;
		fputc(c, out);
	}
}

// arg_get_id: return the matching arguement ID (int) or 0
int arg_get_id (char *opt, char arguments[255][3], int numargs)
{
	int i;

	while (i<=numargs)
		if (strcmp(opt, arguments[i]) )
			return i;

	return 0;
}

// init_char_arr: initialize the array of values to 0
void init_int_arr (int *arr, int limit, int type)
{
	int i;
	for (i=0; i<=limit; i++)
		arr[i] = 0;
}

// init_char_arr: initialize the array of values to 0
/*
void init_char_arr (char *arr, int limit, int type)
{
	int i;
	for (i=0; i<=limit; i++)
		arr[i] = 0;
}
*/

// init_arg_arr: initialize the array passed, which will be used to hold the
// file paths
void init_file_char_arr (char arr[], int limit)
{
	int i;
	for (i=0; i<limit; i++)
		arr[i] = 0;
}
