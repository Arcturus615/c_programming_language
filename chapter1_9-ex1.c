/*
 *  Chapter 1.9 Example 1;
 *	Goal: Determine the longest string in an array
 *	
 *	Outline:
 *		while (theres another line)
 *			if (its longer that the prebious longest)
 *				save it
 *				save its length
 *		print the longest line
 */

#include <stdio.h>
#define MAXLINE	1000 // maximum input line size

int mygetline(char line[], int maxline);
void copy(char to[], char from[]);

// Print longest input line
int main ()
{
	int len;				// Current line length
	int max;				// Maximum length seen so far
	char line[MAXLINE]; 	// Current input line
	char longest[MAXLINE];	// Longest line saved

	max = 0;				// Set the current highest line char count to 0
	while ((len = mygetline(line, MAXLINE)) > 0 )	// While the length of the line being processed is greater than 0
		if (len > max)								// Check if the length is higher than the max recorded
		{											// If so:
			max = len;								//  set the "max" variable to the new length
			copy(longest, line);					//	and copy it from the "line" to "longest"
		}
	if (max > 0)									// If the max line length is greater than 0,
		printf("%s",longest);						// print the line (longest)
	return 0;										// Return successful
}


// getline: Read a line into s, return length
int mygetline(char s[], int lim) // Declare a function "mygetline" with tw argumments, an array "s" and an int lim
/*
 *	s[]: An array that will be used to store the current operating line from stdin
 *	lim: The upper line limit allowed to be processed. Any chars past the limit will be discarded
 *
 */
{
	int c, i; // Declare the ints "c" and "i"
	for (i=0; i<lim-1 && (c=getchar()) != EOF && c!='\n'; ++i)
		// For every character in stdin, unless the character is EOF or \0 (meaning zero line count)
		s[i] = c;	// store the line in a new index in "s" this is why there is a "MAXLINE", as C does not allow
					// C does not allow adding indexes to an array after it has been declared.
	if (c == '\n')	// If the character neing processed is a newline
	{				// then
		s[i] = c;	// assign it to the next index, as normal
		++i;		// and iterate
	}				
	s[i] = '\0';	// Assign a "zero length" indiciator to the proceeding index in the line "s" (actually, line) array
	return i;
}

// copy: copy 'from' into 'to'; assume to is big enough
void copy (char to[], char from[])
	/*
	 * to: The array in which to copy the contents of "from"
	 * from: The array thats being copied
	 *
	 */
{
	int i;	// Declare the local int i

	i = 0; // Start i at index 0
	while ((to[i] = from[i]) != '\0')	// Set the value at index i of "to" to match the same index in from
										// UNLESS the value is '\0'
		++i;							// Iterate to the next index
}
