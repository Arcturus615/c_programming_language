// Goal: Write a program that prints it's input one word per line

#include <stdio.h>

#define IN	1
#define OUT	0

main () {
	int c; // Define vars

	while((c = getchar()) != EOF) {	// Get the current char
		if (c == ' ' || c == '\t')	// If it's either a space or a tab
			c = '\n';				// replace it with a newline
		printf("%c", c);			// and print.
	}
}
