#include <stdio.h>

#define	CHARS	37	// Because I can't be bothered to do this
#define LETTERS	25
#define DIGITS	10

main () {
	int loletters[LETTERS];
	int upletters[LETTERS];
	int digits[DIGITS];
	int blanks, others, chars;

	int c, i, ii;
	
	c = blanks = others = chars = 0;

	for (i = 0; i <= DIGITS; ++i)
		digits[i] = 0;

	for (i = 0; i <= LETTERS; ++i) {
		loletters[i] = 0;
		upletters[i] = 0;
	}

	while ( (c = getchar()) != EOF ) {
		if (c == ' ' || c == '\t' || c == '\n')
			++blanks;

		else if (c >= 'A' && c <= 'Z')
			++upletters[c-'A'];
		
		else if (c >= 'a' && c <= 'z')
			++loletters[c-'a'];
		
		else if (c >= '0' && c <= '9')
			++digits[c-'0'];
		
		else
			++others;
	}

	for ( i = 0; i <= LETTERS; ++i ) {
		if ( (upletters[i] + loletters[i]) > 0 ) {
			printf("Letter %c: ", i+'A');
			for ( ii = 1; ii <= upletters[i] + loletters[i]; ++ii)
				printf("%c", i+'A');	
			printf("\n");
		}
	}

	for ( i = 0; i <= DIGITS; ++i ) {
		if ( digits[i] > 0 ) {
			printf("Number %c: ", i+'0');
			for ( ii = 1; ii <= digits[i]; ++ii )
				printf("%c", i+'0');
			printf("\n");
		}
	}
}
