#include <stdio.h>

/* count characters in input; 2nd version */
main (){
	/* Version 1
	 * long nc;
	 *
	 *nc = 0;
	 *while (getchar() != EOF)
	 *	++nc;
	 *printf("%ld\n", nc);
	 */
	
	double nc;

	for (nc = 0; getchar() != EOF; ++nc)
		;
	 printf("%1.0f\n", nc);

}
