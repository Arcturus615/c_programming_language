/*
 *	Exercise 1-17: Write a program to print lall input lines that are longer than 80 characters
 */


#include <stdio.h>

#define MAXLINE	1000 // The maximum number of chars allowed
					 // per line
#define MINLINE	10	// The minimum count of chars in a line before
					// the line will be printed

int mygetline (char line[], int maxline);
void printline (char line[]);

int main ()
{
	char line[MAXLINE];
	int i, len;

	while ((len = mygetline(line, MAXLINE)) > 0)
		if (len > MINLINE) {
			printf("%d: ", len);
			for (i=0; i<MAXLINE - 1 && line[i] != '\0'; ++i)
				printf("%c", line[i]);
			printf("\n");
		}
}

int mygetline (char s[], int limit)
{
	int c, i;
	for  (i=0; i<limit-1 && (c = getchar()) != EOF && c != '\n'; ++i)
		s[i] = c;
	if (c = '\n') {
		s[i] = c;
		++i;
	}
	s[i] = '\0';
	return i;
}
