/*
 *  Exercise 1.22:
 *  Write a program to "fold" long input lines into two or more shorter lines after
 *  the last non-blank character that occurs before teh nth column of input. Make
 *  sure your program does something intelligent with verylong lines, and if there
 *  are no blanks or tabs before the specified column.
 */

#include <stdio.h>

#define TABSTOP 8
#define TABCOL 6
#define MAXINPUT 32767
#define OUT 0
#define IN 1

int get_line (char line[], int limit, int tabstop);
int find_last_blank (char arr[], int array_limit, int start_index);
void init_array (char array[], int limit);
void print_line (char line[], int limit, int tabstop);
void wrap_tabcol (char line[], char wline[], int limit, int tabstop, int colsetting);


int main ()
{
	int limit = MAXINPUT;
	int tabstop = TABSTOP;
	int tabcol = TABCOL;
	int len = 0;

	char line[limit];
	char wline[limit];

	while ( (len = get_line(line, limit, tabstop)) > 0 )
	{
		wrap_tabcol(line, wline, limit, tabstop, tabcol);
		print_line(wline, limit, tabstop);	
	}

	return 0;
}

// wrap_tabcol: wrap text past a certain tab dillineated column to the previous
// white space in the given character array. Returns a new char array with the
// processed text.
void wrap_tabcol (char s[], char ws[], int lim, int tabset, int colset)
{
	init_array(ws, lim);

	int i, wi, loc;
	wi = i = loc = 0;

	while (s[i] != '\0')
	{
		if ( (i/tabset) >= colset )
		{
			if ( find_last_blank(ws, lim, i) != -1 )
				ws[loc] = '\n';
			else
				ws[wi] = '\n';
			
			wi++;
		}

		ws[wi] = s[i];
		wi++;
		i++;
	}
}

// get_line: assign chars from stdin into a char array for processing.
int get_line (char s[], int lim, int tab)
{
	init_array(s, lim);
	
	char c = 0;
	int i = 0;
	int len = 0;

	while (i < lim && c != EOF)
	{
		c = getchar();
		s[i] = c;

		if (c = '\n')
			c = EOF;

		len++;
		i++;
	}

	s[i] = '\0';
	return len;
}

// find_last_blank: find the previous whitespace, and return
// the index in which it began.
int find_last_blank (char s[], int lim, int i)
{
	char c;
	int ret = -1;

	// Find the previous whitespace from the index given
	while (i < lim && i >= 0 && ret == -1)
	{
		if ( s[i] == ' ' || s[i] == '\t')
		{
			c = s[i];
			ret = i;
		}
		i--;
	}

	// If any whitespace was found (and it's index was greater that 0), find
	// where this block of whitespace began, if there is any further contigious
	// whitespace present.
	while (ret != -1 && i > 0 && ( c == ' ' || c == '\t'))
	{
		c = s[i];
		if (c == ' ' || c == '\t' )
			ret--;
		i--;	
	}

	return ret;
}

// print_line: given a char array, print it.
void print_line (char s[], int lim, int tab)
{
	int i = 0;

	while (s[i] != 0)
	{
		putchar(s[i]);
		i++;
	}
}

// init_array: set all values in the given array to 0
void init_array (char s[], int lim)
{
	int i;
	for (i = 0; i < lim; i++)
